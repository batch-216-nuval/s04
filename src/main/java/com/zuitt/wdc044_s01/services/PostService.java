package com.zuitt.wdc044_s01.services;

import com.zuitt.wdc044_s01.models.Post;



public interface PostService {
    void createPost(String stringToken, Post post);
    Iterable<Post> getPosts();



}
